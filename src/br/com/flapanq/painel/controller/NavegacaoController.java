package br.com.flapanq.painel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NavegacaoController {

	@RequestMapping(value = "/loginpainel")
	public String goToLogin() {
		return "login-painel";
	}

	@RequestMapping(value = "/painelinicial")
	public String goToPainelInicial() {
		return "painel-inicial";
	}

	@RequestMapping(value = "/gerenciarimagens")
	public String goToGerenciarImagens(@RequestParam("id") Long id,
			ModelMap model) {
		model.addAttribute("idNoticia", id);
		return "gerenciar-imagens";
	}
	
	@RequestMapping(value = "/noticias")
	public String goToNoticias(){
		return "noticias";
	}

}
