package br.com.flapanq.painel.controller;

import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.flapanq.contas.ajax.StatusRequest;
import br.com.flapanq.contas.modelo.Noticia;
import br.com.flapanq.contas.modelo.dao.NoticiaDao;

@RestController
public class NoticiaController {
	
	private NoticiaDao noticiaDao;
	
	@Autowired
	public NoticiaController(NoticiaDao noticiaDao) {
		this.noticiaDao = noticiaDao;
	}

	@RequestMapping("cadastar-noticia")
	public StatusRequest cadastrar(@RequestBody Noticia noticia){
		StatusRequest statusRequest = new StatusRequest();
		try {
			noticiaDao.cadastrar(noticia);
            statusRequest.setErro(false);		
		}catch (Exception exception){
			statusRequest.setErro(true);		
		}
		return statusRequest;
		
		
	}
	
	@RequestMapping("listar-noticia")
	public List<Noticia> listarNoticia(){
		return noticiaDao.listarTodas();
	}
	
	
	@RequestMapping("get-noticia")
	public Noticia getNoticia(@RequestParam("id") Long id){
		
		List<Noticia> listaNoticia = noticiaDao.listarTodas();
		
		for(Noticia not : listaNoticia){
			if(not.getId().equals(id)){
				return  not;
			}
		}
		
		return null;
		
	}
	
	
	@RequestMapping("alterar-noticia")
	public StatusRequest alterarNoticia(@RequestBody Noticia noticia){
		
		StatusRequest statusRequest = new StatusRequest();
		try {
			
			// List iterator needed for replacing elements
		    ListIterator<Noticia> listIterator = noticiaDao.listarTodas().listIterator();

		    if (listIterator.hasNext()) {
		      // Need to call next, before set.
		      listIterator.next();
		      // Replace item returned from next()
		      listIterator.set(noticia);
		    }
			
            statusRequest.setErro(false);		
		}catch (Exception exception){
			statusRequest.setErro(true);		
		}
		return statusRequest;
		
	}
	
	
	
	
	
}
