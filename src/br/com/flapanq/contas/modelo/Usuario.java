package br.com.flapanq.contas.modelo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Usuario {
	
	private Integer id;
	
	@Size(max=7 , message = "M�ximo 7 caracteres")
	@NotNull(message = "Preencha o campo nome")
	@NotBlank(message = "Preencha o campo nome")
	private String login;
	private String senha;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
