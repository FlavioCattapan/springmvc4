package br.com.flapanq.contas.modelo;

public enum TipoDaConta {
	ENTRADA,
	SAIDA
}
