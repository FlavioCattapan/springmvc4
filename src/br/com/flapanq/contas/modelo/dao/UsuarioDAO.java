package br.com.flapanq.contas.modelo.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import br.com.flapanq.contas.modelo.Usuario;

@Component
public class UsuarioDAO {

	private static Map<String, Usuario> banco = new HashMap<String, Usuario>();

	static {

		Usuario usuario = new Usuario();
		usuario.setId(1);
		usuario.setLogin("flavio");
		usuario.setSenha("123");
		banco.put(usuario.getLogin(), usuario);

	}

	public UsuarioDAO() {

	}

	public boolean existeUsuario(Usuario usuario) {

		if (usuario == null) {
			throw new IllegalArgumentException("Usu�rio nao deve ser nulo");
		}

		Usuario usuarioRecuperado = banco.get(usuario.getLogin());

		if (usuarioRecuperado != null && usuarioRecuperado.getSenha().equals(usuario.getSenha())) {
			return true;
		} else {
			return false;
		}

	}

	public void insere(Usuario usuario) {
		
		banco.put(usuario.getLogin(), usuario);
		
	}
}
