package br.com.flapanq.contas.modelo;

import java.util.Calendar;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

public class Conta {
	
	
	public Conta() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Conta(Integer id, String descricao, boolean paga, double valor,
			Calendar dataPagamento, TipoDaConta tipo) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.paga = paga;
		this.valor = valor;
		this.dataPagamento = dataPagamento;
		this.tipo = tipo;
	}

	private Integer id;

	@NotBlank(message="{valida.conta.descricao.vasia}")
	@NotNull
	@Size(min=3, message="{valida.conta.descricao}")
	private String descricao;

	private boolean paga;
	
	private double valor;

	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Calendar dataPagamento;
	
	private TipoDaConta tipo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isPaga() {
		return paga;
	}

	public void setPaga(boolean paga) {
		this.paga = paga;
	}

	public Calendar getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Calendar dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public TipoDaConta getTipo() {
		return tipo;
	}

	public void setTipo(TipoDaConta tipo) {
		this.tipo = tipo;
	}

	public double getValor() {
		return valor;
	}	
	
	public void setValor(double valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	

}
