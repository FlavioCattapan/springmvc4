package br.com.flapanq.contas.modelo.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.flapanq.contas.modelo.Conta;
import br.com.flapanq.contas.modelo.dao.ContaDAO;

@Controller
public class ContaController {
	
	private ContaDAO contaDAO;
	
	@Autowired
	public ContaController(ContaDAO contaDAO) {
          this.contaDAO = contaDAO;
	
	}

	@RequestMapping("/form")
	public String formulario() {
		return "conta/form";
	}

	@RequestMapping("/pagaConta")
	public void pagaConta(Conta conta, HttpServletResponse httpServletResponse) {
		contaDAO.paga(conta.getId());
		httpServletResponse.setStatus(200);
	}

	@RequestMapping("/removeConta")
	public String removeConta(Conta conta) {
		contaDAO.remove(conta);
		// http://localhost:8080/SpringMVC4/views/removeConta?id=2
		// return "forward:listaContas";
		// Evita que a uma requisi��o seja enviada duas vezes quando atualizar o browser
		// http://localhost:8080/SpringMVC4/views/listaContas
		// redirect:/ redireciona para a raiz do projeto
		return "redirect:listaContas";
	}

	@RequestMapping("/adicionaConta")
	public String adiciona(@Valid Conta conta, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return "conta/form";
		}

		contaDAO.adiciona(conta);
		return "conta/conta-adicionada";
	}

	@RequestMapping("/listaContas")
	public ModelAndView listaContas() {
		
		//Fornecer ao Dispatcher Servlet os dados que
		//estar�o dispon�veis para a camada de visualiza��o e, ainda mais importante,
		//qual p�gina da nossa aplica��o dever� ser renderizada
		
		ModelAndView modelAndView = new ModelAndView("conta/lista");
		modelAndView.addObject("contas", contaDAO.lista());

		return modelAndView;
	}

	@RequestMapping("/template")
	public String baseTemplate() {

		return "/template/template";
	}

}
