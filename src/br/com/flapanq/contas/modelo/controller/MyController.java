package br.com.flapanq.contas.modelo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

// indica que � um controller MVC
@Controller
public class MyController {
	
	// uri digitada no navegador
	@RequestMapping("mycontroller")
	public String execute(){
		return "ok";
	}
	

}
