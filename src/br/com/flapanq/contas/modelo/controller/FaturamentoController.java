package br.com.flapanq.contas.modelo.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.flapanq.contas.modelo.Faturamento;
import br.com.flapanq.contas.modelo.Usuario;

@Controller
// recuperando dados da sess�o
@SessionAttributes("usuarioLogado")
public class FaturamentoController {

	@RequestMapping("/adicionaFaturamento")
	// recuperando dados da sess�o
	public ModelAndView adicionaFaturamento(@ModelAttribute("usuarioLogado") Usuario usuario, Map<String, Object> model){
		
		if(model.get("faturamento") == null ){
			model.put("faturamento", new Faturamento());
		}
		
		ModelAndView modelAndView = new ModelAndView("faturamento/form");
		modelAndView.getModel().put("usuario", usuario);
		return modelAndView ;
	}
	
	// incluir um par�metro na assinatura do
	//	m�todo alvo, cujo tipo corresponda ao objeto inicialmente presente no modelo,
	//	e todo o processo de liga��o � feito de forma transparente pelo framework.
	@RequestMapping(value = "/salvaFaturamento", method = RequestMethod.POST)
	public ModelAndView salvaFaturamento(@ModelAttribute("usuarioLogado") Usuario usuario, Faturamento faturamento){
		
		ModelAndView modelAndView = new ModelAndView("faturamento/form");
		modelAndView.getModel().put("usuario", usuario);
		
		return modelAndView;
		
		
	}
	
	
}
