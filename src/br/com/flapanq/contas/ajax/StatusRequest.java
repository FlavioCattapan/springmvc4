package br.com.flapanq.contas.ajax;

public class StatusRequest {

	private boolean erro;

	public boolean isErro() {
		return erro;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}
	
}
