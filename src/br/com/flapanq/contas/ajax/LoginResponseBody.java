package br.com.flapanq.contas.ajax;

import com.fasterxml.jackson.annotation.JsonView;

public class LoginResponseBody {
	
	@JsonView(Views.Public.class)
	private boolean logado;

	public boolean isLogado() {
		return logado;
	}

	public void setLogado(boolean logado) {
		this.logado = logado;
	}
	
	

}
