<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<br />

<div class="panel panel-default">
	<div class="panel-heading">Lista Contas</div>
	<div class="panel-body">

		<table class="table table-striped table-bordered table-hover dataTable no-footer"
			id="dataTables-example">
			<tr>
				<th>C�digo</th>
				<th>Descri��o</th>
				<th>Valor</th>
				<th>Tipo</th>
				<th>Paga ?</th>
				<th>Data Pagamento</th>
				<th>A��es</th>
			</tr>
			<!-- 			recuperar os objetos dispon�veis  -->
			<c:forEach items="${contas}" var="conta">
				<tr>
					<td>${conta.id}</td>
					<td>${conta.descricao}</td>
					<td>${conta.valor}</td>
					<td>${conta.tipo}</td>
					<td><c:if test="${conta.paga eq false}"> N�o Paga</c:if> <c:if
							test="${conta.paga eq true}">Paga</c:if></td>
					<td><fmt:formatDate value="${conta.dataPagamento.time}"
							pattern="dd/MM/yyyy" /></td>
					<td>
					<a href="removeConta?id=${conta.id}">Deleta</a> |
					<c:if test="${conta.paga eq false}">
					<a href="#" onclick="paraAgora(${conta.id});" >Pagar</a>
					</c:if>
					</td>		
				</tr>
			</c:forEach>


		</table>



	</div>
</div>


