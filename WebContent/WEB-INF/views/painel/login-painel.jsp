<div  ng-app="app" >
<div ng-controller="LoginController" >
	
		 <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                    
                        <div class="page-header">
                            <h3>Efetue Login</h3>
                        </div>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12 col-sm-7">
                        
                        <form class="form-horizontal" ng-submit="fazerLogin()">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputEmail3" placeholder="Usu�rio" ng-model="login.usuario" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                              <input type="password" class="form-control" id="inputPassword3" placeholder="Senha" ng-model="login.senha" required>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Efetuar Login</button>
                            </div>
                          </div>
                        </form>
                        
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <h4>Instru��es de login</h4>
                        <p>Cuide letras maiusculas e minusculas</p>
                    </div>
                </div>
                
            </div>
            
</div>
</div>

<script type="text/javascript" src="resources/js/controller/LoginController.js"></script>