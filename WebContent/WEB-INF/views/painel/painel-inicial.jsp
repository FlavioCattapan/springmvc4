<div  ng-app="app" >
<div ng-controller="PainelInicialController" >

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#"> Painel de Not�cias </a>
				</div>
			</div>
		</nav>

		<div   class="container">
			<div class="row" >
				<div class="col-xs-12" >
					<div class="well well-sm" >
						<button type="button" class="btn btn-primary" ng-click="abreCadastronoticia()" > Cadastrar Not�cia</button>
					</div>
				</div>			
			</div>
		</div>
		
		<!-- Form cadastro noticias -->
		<div class="container" ng-show="showCadastro" >
			<form ng-submit="processaFormNoticia()" >
			 	<div class="col-xs-3 text-right" >
			 		T�tulo :
			 	</div>
			 	<div class="col-xs-9 row mbotton"   >
			 		<input type="text" 
			 		       class="form-control"
			 		       ng-model="noticia.titulo" 
			 		       required   />
			 	</div>
			 	<div class="col-xs-3 text-right" >
			 		Descri��o :
			 	</div>
			 	<div class="col-xs-9 row mbotton" >
			 		<input type="text" 
			 		       class="form-control" 
			 		       ng-model="noticia.descricao"
			 		       required   />
			 	</div>
			 	<div class="col-xs-3 text-right" >
			 		Date :
			 	</div>
			 	<div class="col-xs-9 row mbotton" >
			 		<input  
			 		       class="form-control" 
			 		       ui-mask="99-99-9999" 
			 		       ng-model="noticia.data" 
			 		       required   />
			 	</div>
			 	<div class="col-xs-3 text-right" >
			 		Text :
			 	</div>
			 	<div class="col-xs-9 row mbotton" >
					<textarea class="form-control" 
					          ng-model="noticia.texto"
					          rows="5"  > </textarea>
			 	</div>
			 	<div class="row mbotton" >
			 		<div class="col-xs-9 col-xs-offset-3" >
			 			<button type="submit" class="btn btn-danger" ng-show="noticia.id==-1"  >Cadastrar</button>
			 			<button type="submit" class="btn btn-success" ng-show="noticia.id!=-1" ng-click="" >Alterar</button>
			 		</div>
			 	</div>
			</form>
		</div>
		<!-- Fim Form cadastro noticias -->
		
		<div class="container" >
			<div class="row" >
				<div class="col-cs-12" >
					<table class="table table-bordered table-striped table-hover"  >
						<thead>
							<tr>
								<th>Data</th>
								<th>T�tulo</th>
								<th width="60" >Bloquear</th>
							    <th width="150" >-</th>
							</tr>
						</thead>
						
						<tbody>
							<tr  ng-repeat="not in allNoticias" >
								<td>{{not.data}}</td>							
								<td>{{not.titulo}}</td>							
								<td>
									<button type="button" class="btn btn_default"  >Bloquear</button>
								</td>							
								<td>
									<button  type="button"  class="btn btn-danger"  >
									 <i  class="glyphicon glyphicon-trash"> </i>
									</button>
									<button  type="button" ng-click="getNoticia(not.id)" class="btn btn-default"  >
									<i class="glyphicon glyphicon-edit"> </i>
									</button>
								    <a href="/SpringMVC4/views/gerenciarimagens?id={{not.id}}" class="btn btn-default" >
								     <i class="glyphicon glyphicon-picture"> </i>
								    </a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>			
			</div>
		</div>

		<div class="alert alert-warning" ng-show="allNoticias.length==0">
			<strong>Nenhuma not�cia cadastrada</strong>
		</div>


	</div>
<script type="text/javascript" src="resources/js/controller/PainelInicialController.js"></script>