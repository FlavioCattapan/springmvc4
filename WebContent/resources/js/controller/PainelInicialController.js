var app = angular.module("app", ['ui.mask', 'angular-loading-bar']);

app.controller('PainelInicialController', function($scope, $http) {

	$scope.showCadastro = false;
	$scope.noticia = objNoticia();
	$scope.allNoticias = [];
	
	$scope.abreCadastronoticia = function() {
		$scope.showCadastro = true;
		$scope.noticia = objNoticia();
		
	};
	
	$scope.processaFormNoticia = function (){
		
		if($scope.noticia.id == -1 ){
			$scope.cadastrarNoticia();
		}else {
			$scope.alterarNoticia();
		}
		
		
	}
	
	$scope.alterarNoticia = function() {
		$http
			.post("/SpringMVC4/views/alterar-noticia", $scope.noticia )
			.success(function(data) {
				if(!data.erro){
					 $.gritter.add({
	                        title : "Sucesso!",
	                        text : "Notícia alterada com sucesso!",
	                        class_name : "gritter"
	                    });	
					$scope.showCadastro = false;
					$scope.noticia = objNoticia();
					$scope.listarNoticias();
				}else {
					$.gritter.add({
                        title : "Falha!",
                        text : "Ocorreu um erro!",
                        class_name : "gritter"
                    });
				}
			})
			.error(function() {
				alert("Falha na aplicação");
			});
	};
	
	
	
	$scope.cadastrarNoticia = function() {
		$http
			.post("/SpringMVC4/views/cadastar-noticia", $scope.noticia )
			.success(function(data) {
				if(!data.erro){
					$scope.listarNoticias();
					 $.gritter.add({
	                        title : "Sucesso!",
	                        text : "Notícia cadastrada com sucesso!",
	                        class_name : "gritter"
	                    });	
					$scope.showCadastro = false;
					$scope.noticia = objNoticia();
				}else {
					$.gritter.add({
                        title : "Falha!",
                        text : "Ocorreu um erro!",
                        class_name : "gritter"
                    });
				}
			})
			.error(function() {
				alert("Falha na aplicação");
			});
	};
	
	$scope.listarNoticias = function() {
		$http
			.get('/SpringMVC4/views/listar-noticia')
			.success(function(data) {
				   $scope.allNoticias = data;
				   console.log($scope.allNotiticas);
			})
			.error(function() {
				alert("Falha na aplicação");
			});
	};
	
	$scope.getNoticia = function(id) {
		$http
		.get('/SpringMVC4/views/get-noticia', { params: { id: id }})
		.success(function(data) {
			   $scope.showCadastro = true;
			   $scope.noticia = data;
		})
		.error(function() {
			alert("Falha na aplicação");
		})
	};
	
	$scope.listarNoticias();
	
});

function objNoticia() {
	return {
		id : -1,
		titulo : "",
		descricao : "",
		texto : "",
		data : ""
	};
}
