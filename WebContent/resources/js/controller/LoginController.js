var app = angular.module('app', []);

app.controller('LoginController', function($scope, $http) {

	$scope.login = {
		usuario : "",
		senha : ""
	};

	$scope.fazerLogin = function() {

		if ($scope.login.usuario.trim() == ""
				|| $scope.login.senha.trim() == "") {
			alert("Informe usuário e senha!");
			return;
		}
		
		$http.post('/SpringMVC4/views/logarpainel', $scope.login)
		.success(function(data){
			if(data.logado){
				window.location = "/SpringMVC4/views/painelinicial";
			}else {
				alert('Verifique o usuário e senha');
			}
			
		});

	};
});
